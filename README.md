# Welcome to the QCP new code library #

This repository stores scripts and files for use in QCP projects.

### Getting started ###

* Browse the files in the library using the files above. 
* Copy files by using "Raw" -> "Save link as".
* Search the library using the search icon in the blue toolbar on the far left-hand side.

### Further instructions ###

* Download the [manual](https://bitbucket.org/qcpadmin/qcpcodelibrary/raw/4ae2a4234461a708a119fe924d36f3abe365e7e6/CodeLibrary_Manual.pdf)

### Who do I talk to? ###

Contact the code library admin.